#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
# aminoacidos para corroborar existencai al ingresar
aminoacidos = {"histidine":"C6H9N3O2",
                "isoleucine":"C6H13NO2",
                "leucine":"C6H13NO2",
                "lysine":"C6H14N2O2",
                "methionine ":"C5H11NO2S",
                "phenylalanine":"C9H11NO2",
                "threonine":"C4H9NO3",
                "tryptophan":"C11H12N2O2",
                "valine":"C5H11NO2",
                "alanine":"C3H7NO2",
                "glutamic acid":"C5H9NO4",
                "aspartic acid":"C4H7NO4",
                "asparagine":"C4H8N2O3",
                "arginine":"C6H14N4O2",
                "cysteine":"C3H7NO2S",
                "glutamine":"C5H10N2O3",
                "tyrosine":"C9H11NO3",
                "glycine":"C2H5NO2",
                "proline":"C5H9NO2",
                "serine":"C3H7NO3"}
# diccionario vacío
amino_archivo = {}

# ~ def amino_buscar():
# función elimina o edita aminoacido y formula
def edit_delete(amino_archivo):
    # se abre el archivo
    with open('amino_archivo.json', 'r') as archivo:
        aminoacidos = json.load(archivo)
    # ~ print(aminoacidos)
    aminos = list(aminoacidos)
    # si no se encuentran datos
    #retorna a menu
    if len(aminos) == 0:
        print("NO HAY DATOS")
        menu()
    else:
        a = input("Que desea modificar? aminoacido(a) o formula(f): ")
        if a == 'a':
            amino = input("Ingrese aminoacido: ")
            elec = input("Borrar (b)/ Editar(e): ")
            if elec == 'b':
                for i in aminoacidos:
                    # ~ print (i)
                    if amino == i:
                        # se elimina el dato ingresado igualado en el archivo
                        del aminoacidos[i]
                        with open("amino_archivo.json", 'w') as file:
                            json.dump(aminoacidos, file)
                        print("Aminoacido eliminado")
                        menu()
                    else:
                        print("El aminoacido ingresado no existe")
                        edit_delete(amino_archivo)
            elif elec == 'e':
                amino_nuevo = input("Ingrese nuevo aminoacido: ")
                for i in aminoacidos:
                    if amino == i:
                        # se modifica con un nuevo ingreso
                        aminoacidos[amino_nuevo] = aminoacidos.pop(amino)
                        with open("amino_archivo.json", 'w') as file:
                            json.dump(aminoacidos, file)
                        print("Aminoacido editado")
                        menu()
                    else:
                        print("El aminoacido ingresado no existe")
        elif a == 'f':
            # edita formula ingresada
            form = input("Ingrese fórmula: ")
            form_nueva = input("Ingrese nueva fórmula: ")
            # cambia antigua por nueva
            aminoacidos[form] = form_nueva
            with open("amino_archivo.json", 'w') as file:
                json.dump(aminoacidos, file)
                print("Fórmula editada")

# función guarda datos en archivo
def guardar_archivo():
    with open("amino_archivo.json", 'w') as file:
        json.dump(amino_archivo, file)

# función verifica existencia de aminoacidos
def verificacion(aminoacidos, conjunto):
    aminos = list(aminoacidos)
    a_ingresado = list(conjunto)
    for a in a_ingresado:
        cont = 0
        for amino in aminos:
            if a == amino:
                cont = 1
    if cont == 1:
        print("Aminoacido correcto")
    else:
        print("Aminoacido incorrecto")
    # se transforman a listas para uso más facil
    formula = list(aminoacidos.values())
    f_ingresada = list(conjunto.values())
    for f in f_ingresada:
        cont = 0
        for form in formula:
            if f == form:
                cont = 1
    if cont == 1:
        print("Fórmula correcta")
    else:
        print("Fórmula incorrecta")
    print(a_ingresado,"|", f_ingresada)

# funcóin permite visualizar progreso
def amino_vizualizar():
    with open('amino_archivo.json', 'r') as archivo:
        aminoacidos = json.load(archivo)
    # como listas
    aminos = list(aminoacidos)
    formulas = list(aminoacidos.values())
    for i in range(len(aminoacidos)):
        print(aminos[i],":", formulas[i])

# función para ingresar aminoacidos y formulas
def amino_insertar(amino_archivo):
    conjunto = {input(" Ingrese aminoacido: "): input("Ingrese formula: ")}
    amino_archivo.update(conjunto)
    # se verifica
    verificacion(aminoacidos, conjunto)
    # ~ print(conjunto)
    # ~ print(amino_archivo)
    # se guarda
    guardar_archivo()
    menu()

# función menú
def menu():
    print("_______________________________________")
    print("|---------INGRESE UNA OPCIÓN-----------|")
    print("|                                      |")
    print("|   1) Insertar aminoácido             |")
    print("|   2) Buscar aminoácido               |")
    print("|   3) Eliminar o Editar               |")
    print("|   4) Visualizar                      |")
    print("|   5) Salir                           |")
    print("|______________________________________|")
    try:
        opcion = int(input("-----> "))
        if opcion == 1:
            amino_insertar(amino_archivo)
            menu()
        elif opcion == 2:
            print("compre el dlc para esta opción")
            # ~ amino_buscar()
            menu()
        elif opcion == 3:
            edit_delete(amino_archivo)
            menu()
        elif opcion == 4:
            amino_vizualizar()
            menu()
        elif opcion == 5:
            print("          \ SHAO MI REY /")
        else:
            menu()
    except ValueError:
        menu()
# llama a menú
if __name__ == "__main__":
    print(" *****BASE DE DATOS DE AMINOÁCIDOS*****")
    menu()
