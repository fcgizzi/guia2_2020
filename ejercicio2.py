#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Ejercicio 2
    Se pide poder leer el archivo covid_19_data y obtener ciertas estadisticas basicas:
    -Contar y mostrar los países que hasta el momento cuentan con a lo menos un contagiado.
    -Listar de cada País el total de infectados confirmados.
    -Suma cuantos recuperados y cuantos muertos por país existen hasta el momento de generar este
    archivo.
"""

# La mayoría de las funciones tienen el mismo concepto, buscar en el archivo
# y crear listas que posteriormente serán usadas para extraer uno por uno
# el dato que se requiera para mostrar

# Función creadora de lista de recuperados
def contador_recuperados(paises):
    # lista vacía
    recuperados = []
    for pais in paises:
        contador = 0
        covid_archivo = open('covid_19_data.csv')
        # se busca en el archivo limitando con la fecha
        for i, linea in enumerate(covid_archivo):
            linea = linea.split(',')
            if linea[1] == '03/27/2020':
                # cuando los paíse coinciden
                if pais == linea[3]:
                    # se suma en un contador temporal
                    # en este caso de suman los recuperados,
                    # por ende se suman los datos de la linea [7]
                    contador += float(linea[7])
        # se transforman los datos float a datos enteros
        total = "{0:.0f}".format(contador)
        # y se añaden en la lista vacía
        recuperados.append(total)
    # ~ print(recuperados)
    covid_archivo.close()
    # retorna la lista de recuperados
    return recuperados

# Función creadora de lista de muertos
def contador_muertos(paises):
    # lista vacía
    muertos = []
    # se usa la lista de paises
    for pais in paises:
        contador = 0
        covid_archivo = open('covid_19_data.csv')
        # se busca en el archivo limitando con la fecha
        for i, linea in enumerate(covid_archivo):
            linea = linea.split(',')
            if linea[1] == '03/27/2020':
                # cuando los países coinciden
                if pais == linea[3]:
                    # se suma en un contador temporal
                    # en este caso de suman los muertos,
                    # por ende se suman los datos de la linea [6]
                    contador += float(linea[6])
        # se transforman los datos float a datos enteros
        total = "{0:.0f}".format(contador)
        # y se añaden en la lista vacía
        muertos.append(total)
    # ~ print(muertos)
    covid_archivo.close()
    # retorna lista de muertos
    return muertos

# Función creadora de lista de contagiados en paises
def casos_confirmados(paises):
    # lista vacía
    confirmados = []
    for pais in paises:
        contador = 0
        covid_archivo = open('covid_19_data.csv')
        # se busca en el archivo limitando con la fecha
        for i, linea in enumerate(covid_archivo):
            linea = linea.split(',')
            if linea[1] == '03/27/2020':
                # si el pais se encuentra, el contador suma sus contagiados
                if pais == linea[3]:
                    contador += float(linea[5])
        # se transforman los datos float a datos enteros
        total = "{0:.0f}".format(contador)
        # y se añaden en la lista vacía
        confirmados.append(total)
    # ~ print(contagiados)
    covid_archivo.close()
    # retorna lista de casos confirmados de contagio
    return confirmados

# Esta función crea una lista que se usa en las otras funciones
# La lista de paises totales con contagiados
def paises_contagiados(covid_archivo):
    # lista vacía
    paises = []
    # se recorre el archivo desde la segunda fila, ya que la primera no nos sirve
    for i, linea in enumerate(covid_archivo):
        linea = linea.split(',')
        if i > 0:
            # se busca en el archivo limitando con la fecha
            if linea[1] == '03/27/2020':
                contador = 0
                # ciclo en que se agregan los paises
                # a la nueva lista de paises vacía
                for i in range(len(paises)):
                    if paises[i] == linea[3]:
                        contador += 1
                # cuando se repita un valor el contador sumará
                # y el dato repetido no se agregará, además,
                # como está vacía, solo se agregará una vez
                if contador == 0:
                    paises.append(linea[3])
    # retorna la lista de países
    return paises

# Función menú
if __name__ == "__main__":
    covid_archivo = open('covid_19_data.csv')
    paises = paises_contagiados(covid_archivo)
    # ~ print(paises)
    # se cuentan los países que hay en la lista
    # para encontrar el total de países contagiados
    contagiados = len(paises)
    confirmados = casos_confirmados(paises)
    muertos = contador_muertos(paises)
    recuperados = contador_recuperados(paises)
    covid_archivo.close()
    print(" LISTA DE PAISES CONTAGIADOS POR COVID-19 (", contagiados, ')')
    # se usa un texto adicional para no usar tanto espacio (pep-8)
    a = "\n {0}: {1} Casos confirmados | {2} Muertos | {3} Recuperados "
    #ciclo recorre el largo de la lista paises
    for i in range(len(paises)):
        # agrega cada valor de cada lista en so respectivo índice
        print(a.format(paises[i], confirmados[i], muertos[i], recuperados[i]))
