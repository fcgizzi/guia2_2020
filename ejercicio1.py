#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    Ejercicio 1
    Cambiar las key por values y values por key del siguiente diccionario:
    # amino - # formula molecular
    dic = {'Histidine': 'C6H9N3O2'}
    Debe quedar del siguiente modo:
    # amino - #f molecular
    nuevo = {'C6 ': 'Histidine ',
             'H9 ': 'Histidine ',
             'N3 ': 'Histidine ',
             'O2 ': 'Histidine '}
"""

# función que cambia key por value y separa los atomos
def cambio(dic, formula):
    # se crea un nuevo diccionario vacío para llenar
    diccionario = {}
    # ciclo que recorre la formula completa saltando de 2 en 2
    for i in range(1, len(formula), 2):
        # se separan los atomos, para que a este se le agregue el nombre del Aa
        atomo = formula[i - 1] + formula[i]
        # ~ print(atomo)
        # se juntan en pequeños diccionarios temporales
        dictemp = {atomo: 'Histidine'}
        # ~ print(dictemp)
        # se agregan al diccionario vacío
        diccionario.update(dictemp)
    print("\n NUEVO: \n", diccionario)

# función menú
if __name__ == "__main__":
    dic = {'Histidina': 'C6H9N3O2'}
    print(" ORIGINAL: \n", dic)
    # se extrae la formula usando value
    formula = list(dic.values())[0]
    # ~ print(formula)
    cambio(dic, formula)
